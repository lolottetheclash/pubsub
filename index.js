// subscriber store
let topics = {}

// example:
// topics = {topic1: [subscriber1, subscriber2], topic2: [subscriber3]}


let subscribe = (topic, subscriber) => {
    // initializes topic if not existent
    if (!topics[topic])
        topics[topic] = []
    // push subscriber to topic's listener
    topics[topic].push(subscriber)
    console.log(topics)
}

let publish = (topic, option) => {
    console.log(`topic = ${topic}`)
    console.log(`option = ${option}`)
    topics[topic].forEach(sub => {
        sub(option) 
    });

  //topics[topic].option
  // for each registered subscriber to given topic, call subscriber(option)
}

module.exports = { subscribe: subscribe, publish: publish }

